import { Dispatch, AnyAction } from 'redux';
import { AppState } from '../redux';
import { STORE_VIEWORDERDATA, STORE_ORDERID, STORE_USERCODE } from '../reducers/viewOrder';
import { RowData } from 'src/reducers/viewOrder';

import axios from 'axios';

export const getViewOrderData = (orderId: string | null, usercode: string | null) => {
    return async (
        dispatch: Dispatch<AppState>,
        getState: () => AppState
    ) => {
        dispatch({ type: STORE_ORDERID, orderId } as AnyAction);
        dispatch({ type: STORE_USERCODE, usercode } as AnyAction);

        const viewOrderData = await axios.post(
            `${
            getState().environment.API_URL
            }getOrderDetails?orderId=${orderId}`
        );
        const viewData: RowData[] = await viewOrderData.data;

        dispatch({ type: STORE_VIEWORDERDATA, viewData } as AnyAction);
    };
};

export const onBack = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {

    // const viewData: RowData[] = [];
    // dispatch({ type: STORE_VIEWORDERDATA, viewData } as AnyAction);
    location.href = document.referrer;

};
