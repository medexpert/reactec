import { Dispatch, AnyAction } from 'redux';
import { AppState } from '../redux';
import {
    STORE_CAMERA_OPTIONS,
    STORE_PHOTOFILE_OPTIONS,
    STORE_SERVICE_TYPE_SIMPLE,
    STORE_SERVICE_TYPE_ADVANCED,
    STORE_USERCODE,
    STORE_IMAGES_NORMAL,
    STORE_IMAGES_WIDE,
    STORE_FILE_ZIP,
    STORE_IMAGE_DATA_NORMAL,
    STORE_IMAGE_DATA_WIDE,
    STORE_PLASTNAME,
    STORE_PFIRSTNAME,
    STORE_PAGE,
    STORE_PGENDER
} from '../reducers/order';
import { STORE_SUCCESS_FILES_UPLOADED, STORE_SUCCESS_SUBMIT, STORE_SUCCESS_SUBMIT_MSG, STORE_ERROR_SUBMIT, STORE_ERROR_SUBMIT_MSG } from 'src/reducers/errors';
// STORE_WARN_FILES_SUBMIT

import { push } from 'react-router-redux';

import axios, { AxiosResponse, AxiosError } from 'axios';

// STORE_WARN_FILES_UPLOADED
export const createOrder = (event: React.ChangeEvent<{ value: string }>) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        const orderType = event.target.value as string;
        if (orderType === 'Simple') {
            dispatch(push('/app/smilesimple'));
        }
        if (orderType === 'Advanced') {
            dispatch(push('/app/smileadvanced'));
        }
        return true;
    };
};

// <-------------------------------------------------------------------------------------------------------------------->

export const SavePFirstName = (firstName: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: STORE_PFIRSTNAME, firstName } as AnyAction);
        return true;
    };
};
export const SavePLastName = (lastName: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: STORE_PLASTNAME, lastName } as AnyAction);
        return true;
    };
};
export const SavePAge = (page: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        if (parseInt(page, 10) <= 120 && parseInt(page, 10) > 0) {
            const age: number = parseInt(page, 10);
            dispatch({ type: STORE_PAGE, age } as AnyAction);
        } else {
            console.error('age is not valid');
            const age = '';
            dispatch({ type: STORE_PAGE, age } as AnyAction);
        }
        return true;
    };
};
export const SavePGender = (gender: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: STORE_PGENDER, gender } as AnyAction);
        return true;
    };
};

// <-------------------------------------------------------------------------------------------------------------------->

export const saveCameraOptions = (cameraDisplayOption: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: STORE_CAMERA_OPTIONS, cameraDisplayOption } as AnyAction);
        return true;
    };
};
export const savePhotoOptions = (photoDisplayOption: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: STORE_PHOTOFILE_OPTIONS, photoDisplayOption } as AnyAction);
        return true;
    };
};

// <----------------------------------------------------------------------------------------------------------------------->

export const saveSimpleOptions = (smileDisplayOptions: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: STORE_SERVICE_TYPE_SIMPLE, smileDisplayOptions } as AnyAction);
        return true;
    };
};
export const saveAdvancedOptions = (advancedDisplayOption: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: STORE_SERVICE_TYPE_ADVANCED, advancedDisplayOption } as AnyAction);
        return true;
    };
};

// <---------------------------------------------------------------------------------------------------------------------->

export const onSaveuserCode = (usercode: string | null) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: STORE_USERCODE, usercode } as AnyAction);
    };
};

// <----------------------------------------------------------------------------------------------------------------------->

export const imageAr = (dataUri: string, filename: string, mimeType: string) => {

    const boundary = `----${(new Date()).getTime()}`;
    const bodyString = [];
    bodyString.push(`--${boundary}`, `Content-Disposition: form-data; name="file";filename="${filename}", Content-type"image/jpeg", Content-Transfer-Encoding: base64, ${dataUri.substring(23)}`);
    // remove the beginning of the string data:image/jpeg;base64,  
    bodyString.push(`--${boundary}--, `);
    const content = bodyString.join('\r\n');
    return { content: content, headers: { 'Content-Type': `multipart/form-data; boundary= ${boundary}`, 'Content-Length': content.length } };

};

// <--------------------------------above set of code has not been used--------------------------------->

export const onSaveCameraPicNormal = (dataUri: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        const filename = `Normalsmile_${getState().order.usercode}_${(new Date()).getTime()}.jpg`;

        fetch(dataUri)
            .then(res => res.blob())
            .then(blob => {
                const file: File = new File([blob], filename, { type: 'image/png' });
                const imagedatanormal: Array<File> = [...getState().order.imagedatanormal].concat([file]);
                dispatch({ type: STORE_IMAGE_DATA_NORMAL, imagedatanormal } as AnyAction);
                const imagesNormal: Array<File> = [];
                dispatch({ type: STORE_IMAGES_NORMAL, imagesNormal } as AnyAction);
                const imagesWide: Array<File> = [];
                dispatch({ type: STORE_IMAGES_WIDE, imagesWide } as AnyAction);

            });

    };
};
export const onSaveCameraPicWide = (dataUri: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        const filename = `Widesmile_${getState().order.usercode}_${(new Date()).getTime()}.jpg`;
        fetch(dataUri)
            .then(res => res.blob())
            .then(blob => {
                const file: File = new File([blob], filename, { type: 'image/png' });
                const imagedatawide: Array<File> = [...getState().order.imagedatawide].concat([file]);
                dispatch({ type: STORE_IMAGE_DATA_WIDE, imagedatawide } as AnyAction);
                const imagesNormal: Array<File> = [];
                dispatch({ type: STORE_IMAGES_NORMAL, imagesNormal } as AnyAction);
                const imagesWide: Array<File> = [];
                dispatch({ type: STORE_IMAGES_WIDE, imagesWide } as AnyAction);
            });
    };
};

// <------------------------------------------------------------------------------------------------------------------->

export const onFilesChangeNormalSmile = (images: Array<File>) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        const filterfile: Array<File> = [...getState().order.imagesNormal].concat(images);

        const seen = new Set();
        const imagesNormal = filterfile.filter(el => {
            const duplicate = seen.has(el.name);
            seen.add(el.name);
            return !duplicate;
        });

        if (imagesNormal.length > 0) {
            const successFileUploadOpen: boolean = true;
            dispatch({ type: STORE_SUCCESS_FILES_UPLOADED, successFileUploadOpen } as AnyAction);
            const imagedatanormal: Array<File> = [];
            dispatch({ type: STORE_IMAGE_DATA_NORMAL, imagedatanormal } as AnyAction);
            const imagedatawide: Array<File> = [];
            dispatch({ type: STORE_IMAGE_DATA_WIDE, imagedatawide } as AnyAction);
            dispatch({ type: STORE_IMAGES_NORMAL, imagesNormal } as AnyAction);
            images.length = 0;
        }

    };
};
export const onFilesChangeWideSmile = (images: Array<File>) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        const filterfile: Array<File> = [...getState().order.imagesWide].concat(images);

        const seen = new Set();
        const imagesWide = filterfile.filter(el => {
            const duplicate = seen.has(el.name);
            seen.add(el.name);
            return !duplicate;
        });

        if (imagesWide.length > 0) {
            const successFileUploadOpen: boolean = true;
            dispatch({ type: STORE_SUCCESS_FILES_UPLOADED, successFileUploadOpen } as AnyAction);
            const imagedatanormal: Array<File> = [];
            dispatch({ type: STORE_IMAGE_DATA_NORMAL, imagedatanormal } as AnyAction);
            const imagedatawide: Array<File> = [];
            dispatch({ type: STORE_IMAGE_DATA_WIDE, imagedatawide } as AnyAction);
            dispatch({ type: STORE_IMAGES_WIDE, imagesWide } as AnyAction);
            images.length = 0;
        }

    };
};
export const onFilesChangeStlzip = (fileImages: Array<File>) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {

        const filezip: Array<File> = fileImages;

        if (filezip.length > 0) {
            const successFileUploadOpen: boolean = true;
            dispatch({ type: STORE_SUCCESS_FILES_UPLOADED, successFileUploadOpen } as AnyAction);
            dispatch({ type: STORE_FILE_ZIP, filezip } as AnyAction);
        }
        // fileImages.length = 0;
    };
};

// <------------------------------------------------------------------------------------------------------------------------------->

export const onClearAllFilesSmileb2c = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {

    if (getState().order.imagesNormal > []) {
        const imagesNormal: Array<File> = [];
        dispatch({ type: STORE_IMAGES_NORMAL, imagesNormal } as AnyAction);
    }
    if (getState().order.imagesWide > []) {
        const imagesWide: Array<File> = [];
        dispatch({ type: STORE_IMAGES_WIDE, imagesWide } as AnyAction);
    }
    if (getState().order.imagedatanormal.length > 0 || getState().order.imagedatanormal.length > 0) {
        const imagedatanormal: Array<File> = [];
        const imagedatawide: Array<File> = [];
        dispatch({ type: STORE_IMAGE_DATA_NORMAL, imagedatanormal } as AnyAction);
        dispatch({ type: STORE_IMAGE_DATA_WIDE, imagedatawide } as AnyAction);
    }
    const firstName: string = '';
    const lastName: string = '';
    const age: string = '';
    const gender: string = '';
    dispatch({ type: STORE_PFIRSTNAME, firstName } as AnyAction);
    dispatch({ type: STORE_PLASTNAME, lastName } as AnyAction);
    dispatch({ type: STORE_PAGE, age } as AnyAction);
    dispatch({ type: STORE_PGENDER, gender } as AnyAction);
};
export const onClearAllFilesValidateb2c = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {

    if (getState().order.imagesNormal > []) {
        const imagesNormal: Array<File> = [];
        dispatch({ type: STORE_IMAGES_NORMAL, imagesNormal } as AnyAction);
    }
    if (getState().order.imagesWide > []) {
        const imagesWide: Array<File> = [];
        dispatch({ type: STORE_IMAGES_WIDE, imagesWide } as AnyAction);
    }
    if (getState().order.imagedatanormal.length > 0 || getState().order.imagedatanormal.length > 0) {
        const imagedatanormal: Array<File> = [];
        const imagedatawide: Array<File> = [];
        dispatch({ type: STORE_IMAGE_DATA_NORMAL, imagedatanormal } as AnyAction);
        dispatch({ type: STORE_IMAGE_DATA_WIDE, imagedatawide } as AnyAction);
    }

    if (getState().order.filezip > []) {
        const filezip: Array<File> = [];
        dispatch({ type: STORE_FILE_ZIP, filezip } as AnyAction);
    }
    const firstName: string = '';
    const lastName: string = '';
    const age: string = '';
    const gender: string = '';
    dispatch({ type: STORE_PFIRSTNAME, firstName } as AnyAction);
    dispatch({ type: STORE_PLASTNAME, lastName } as AnyAction);
    dispatch({ type: STORE_PAGE, age } as AnyAction);
    dispatch({ type: STORE_PGENDER, gender } as AnyAction);

};

// <------------------------------------------------------------------------------------------------------------------------------>

export const onUploadImages = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {

    const imagesnormal: Array<File> = [...getState().order.imagesNormal];
    const imageswide: Array<File> = [...getState().order.imagesWide];
    const imageDataNormal: Array<File> = [...getState().order.imagedatanormal];
    const imageDataWide: Array<File> = [...getState().order.imagedatawide];

    if (imagesnormal.length === 1 && imageswide.length === 1 && getState().order.photoDisplayOption === 'flex') {

        const formData = new FormData();

        formData.append('normalsmile', imagesnormal[0]);
        formData.append('widesmile', imageswide[0]);

        await axios({
            method: 'post',
            url: `${getState().environment.API_URL}easyUpload`,
            data: formData,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/multipart/form-data',
                usercode: getState().order.usercode,
                servicetype: '1',
                design: '',
                msg: '',
                material: '',
                tat: '',
                firstname: getState().order.firstName,
                lastname: getState().order.lastName,
                age: getState().order.age,
                gender: getState().order.gender
            },
        }).then(async function (response: AxiosResponse) {

            const imagesNormal: Array<File> = [];
            dispatch({ type: STORE_IMAGES_NORMAL, imagesNormal } as AnyAction);
            const imagesWide: Array<File> = [];
            dispatch({ type: STORE_IMAGES_WIDE, imagesWide } as AnyAction);
            console.error('postive res image', response);
            const successSubmitOpen: boolean = true;
            dispatch({ type: STORE_SUCCESS_SUBMIT, successSubmitOpen } as AnyAction);
            const successsubmitmsg: string = `Your order no: ${response.data.data.caseno} has been placed successfully`;
            dispatch({ type: STORE_SUCCESS_SUBMIT_MSG, successsubmitmsg } as AnyAction);

            const firstName: string = '';
            const lastName: string = '';
            const age: string = '';
            const gender: string = '';
            dispatch({ type: STORE_PFIRSTNAME, firstName } as AnyAction);
            dispatch({ type: STORE_PLASTNAME, lastName } as AnyAction);
            dispatch({ type: STORE_PAGE, age } as AnyAction);
            dispatch({ type: STORE_PGENDER, gender } as AnyAction);
        }).catch(function (error: AxiosError) {
            console.error('err res', error, formData);
            const errorSubmitOpen: boolean = true;
            dispatch({ type: STORE_ERROR_SUBMIT, errorSubmitOpen } as AnyAction);
            const errorsubmitmsg: string = error.message;
            dispatch({ type: STORE_ERROR_SUBMIT_MSG, errorsubmitmsg } as AnyAction);
        });

    }
    //  else {
    //     const warningSubmitOpen: boolean = true;
    //     dispatch({
    //         type: STORE_WARN_FILES_SUBMIT,
    //         warningSubmitOpen
    //     } as AnyAction);
    // }

    if (imageDataNormal.length > 0 && imageDataWide.length > 0 && getState().order.cameraDisplayOption === 'flex') {

        const normalData: File = imageDataNormal[imageDataNormal.length - 1];
        const wideData: File = imageDataWide[imageDataWide.length - 1];

        const formDataS = new FormData();
        formDataS.append('normalsmile', normalData);
        formDataS.append('widesmile', wideData);

        await axios({
            method: 'post',
            url: `${getState().environment.API_URL}easyUpload`,
            data: formDataS,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/multipart/form-data',
                usercode: getState().order.usercode,
                servicetype: '1',
                design: '',
                msg: '',
                material: '',
                tat: '',
                firstname: getState().order.firstName,
                lastname: getState().order.lastName,
                age: getState().order.age,
                gender: getState().order.gender
            },
        }).then(async function (response: AxiosResponse) {

            const responseOrder = await axios.post(`${getState().environment.API_URL}getOrderDetails?orderId=${response.data.data.recordid}`);

            console.error('postive res image', response);
            const successSubmitOpen: boolean = true;
            dispatch({ type: STORE_SUCCESS_SUBMIT, successSubmitOpen } as AnyAction);
            const successsubmitmsg: string = `Your order no: ${responseOrder.data[0].orderno} has been placed successfully`;
            dispatch({ type: STORE_SUCCESS_SUBMIT_MSG, successsubmitmsg } as AnyAction);

            const imagedatanormal: Array<File> = [];
            const imagedatawide: Array<File> = [];
            dispatch({ type: STORE_IMAGE_DATA_NORMAL, imagedatanormal } as AnyAction);
            dispatch({ type: STORE_IMAGE_DATA_WIDE, imagedatawide } as AnyAction);

            const firstName: string = '';
            const lastName: string = '';
            const age: string = '';
            const gender: string = '';
            dispatch({ type: STORE_PFIRSTNAME, firstName } as AnyAction);
            dispatch({ type: STORE_PLASTNAME, lastName } as AnyAction);
            dispatch({ type: STORE_PAGE, age } as AnyAction);
            dispatch({ type: STORE_PGENDER, gender } as AnyAction);
        }).catch(function (error: AxiosError) {
            console.error('err res', error, formDataS);
            const errorSubmitOpen: boolean = true;
            dispatch({ type: STORE_ERROR_SUBMIT, errorSubmitOpen } as AnyAction);
            const errorsubmitmsg: string = error.message;
            dispatch({ type: STORE_ERROR_SUBMIT_MSG, errorsubmitmsg } as AnyAction);
        });

    }
    return true;
};

// <-------------------------------------------------------------------------------------------------------------->

export const onUploadzip = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {

    const imagesnormal: Array<File> = [...getState().order.imagesNormal];
    const imageswide: Array<File> = [...getState().order.imagesWide];
    const imageDataNormal: Array<File> = [...getState().order.imagedatanormal];
    const imageDataWide: Array<File> = [...getState().order.imagedatawide];
    const filesUpload: Array<File> = [...getState().order.filezip];

    if ((imagesnormal.length === 1 && imageswide.length === 1) && (filesUpload.length > 0 && getState().order.photoDisplayOption === 'flex')) {

        const formData = new FormData();

        formData.append('normalsmile', imagesnormal[0]);
        formData.append('widesmile', imageswide[0]);
        formData.append('scanfile', filesUpload[0]);

        await axios({
            method: 'post',
            url: `${getState().environment.API_URL}easyUpload`,
            data: formData,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/multipart/form-data',
                usercode: getState().order.usercode,
                servicetype: '2',
                design: '',
                msg: '',
                material: '',
                tat: '',
                firstname: getState().order.firstName,
                lastname: getState().order.lastName,
                age: getState().order.age,
                gender: getState().order.gender
            },
        }).then(async function (response: AxiosResponse) {
            const responseOrder = await axios.post(`${getState().environment.API_URL}getOrderDetails?orderId=${response.data.data.recordid}`);

            const imagesNormal: Array<File> = [];
            dispatch({ type: STORE_IMAGES_NORMAL, imagesNormal } as AnyAction);
            const imagesWide: Array<File> = [];
            dispatch({ type: STORE_IMAGES_WIDE, imagesWide } as AnyAction);

            const firstName: string = '';
            const lastName: string = '';
            const age: string = '';
            const gender: string = '';
            dispatch({ type: STORE_PFIRSTNAME, firstName } as AnyAction);
            dispatch({ type: STORE_PLASTNAME, lastName } as AnyAction);
            dispatch({ type: STORE_PAGE, age } as AnyAction);
            dispatch({ type: STORE_PGENDER, gender } as AnyAction);

            console.error('postive res image', response);
            const successSubmitOpen: boolean = true;
            dispatch({ type: STORE_SUCCESS_SUBMIT, successSubmitOpen } as AnyAction);
            const successsubmitmsg: string = `Your order no: ${responseOrder.data[0].orderno} has been placed successfully`;
            dispatch({ type: STORE_SUCCESS_SUBMIT_MSG, successsubmitmsg } as AnyAction);
        }).catch(function (error: AxiosError) {
            console.error('err res', error, formData);
            const errorSubmitOpen: boolean = true;
            dispatch({ type: STORE_ERROR_SUBMIT, errorSubmitOpen } as AnyAction);
            const errorsubmitmsg: string = error.message;
            dispatch({ type: STORE_ERROR_SUBMIT_MSG, errorsubmitmsg } as AnyAction);
        });

    }

    if ((imageDataNormal.length > 0 && imageDataWide.length > 0) && (filesUpload.length > 0 && getState().order.cameraDisplayOption === 'flex')) {

        const normalData: File = imageDataNormal[imageDataNormal.length - 1];
        const wideData: File = imageDataWide[imageDataWide.length - 1];

        const formDataS = new FormData();
        formDataS.append('normalsmile', normalData);
        formDataS.append('widesmile', wideData);
        formDataS.append('scanfile', filesUpload[0]);

        await axios({
            method: 'post',
            url: `${getState().environment.API_URL}easyUpload`,
            data: formDataS,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/multipart/form-data',
                usercode: getState().order.usercode,
                servicetype: '2',
                design: '',
                msg: '',
                material: '',
                tat: '',
                firstname: getState().order.firstName,
                lastname: getState().order.lastName,
                age: getState().order.age,
                gender: getState().order.gender
            },
        }).then(async function (response: AxiosResponse) {
            const responseOrder = await axios.post(`${getState().environment.API_URL}getOrderDetails?orderId=${response.data.data.recordid}`);

            console.error('postive res image', response);
            const successSubmitOpen: boolean = true;
            dispatch({ type: STORE_SUCCESS_SUBMIT, successSubmitOpen } as AnyAction);
            const successsubmitmsg: string = `Your order no: ${responseOrder.data[0].orderno} has been placed successfully`;
            dispatch({ type: STORE_SUCCESS_SUBMIT_MSG, successsubmitmsg } as AnyAction);

            const imagedatanormal: Array<File> = [];
            const imagedatawide: Array<File> = [];
            dispatch({ type: STORE_IMAGE_DATA_NORMAL, imagedatanormal } as AnyAction);
            dispatch({ type: STORE_IMAGE_DATA_WIDE, imagedatawide } as AnyAction);
            const filezip: Array<File> = [];
            dispatch({ type: STORE_FILE_ZIP, filezip } as AnyAction);

            const firstName: string = '';
            const lastName: string = '';
            const age: string = '';
            const gender: string = '';
            dispatch({ type: STORE_PFIRSTNAME, firstName } as AnyAction);
            dispatch({ type: STORE_PLASTNAME, lastName } as AnyAction);
            dispatch({ type: STORE_PAGE, age } as AnyAction);
            dispatch({ type: STORE_PGENDER, gender } as AnyAction);
        }).catch(function (error: AxiosError) {
            console.error('err res', error, formDataS);
            const errorSubmitOpen: boolean = true;
            dispatch({ type: STORE_ERROR_SUBMIT, errorSubmitOpen } as AnyAction);
            const errorsubmitmsg: string = error.message;
            dispatch({ type: STORE_ERROR_SUBMIT_MSG, errorsubmitmsg } as AnyAction);
        });

    }
    return true;
};
