import { Dispatch, AnyAction } from 'redux';
import { AppState } from '../redux';
// import { STORE_SUCCESS_FILES_UPLOADED, STORE_SUCCESS_SUBMIT } from 'src/reducers/errors';
// STORE_WARN_FILES_SUBMIT
const FileSaver = require('file-saver');
import axios, { AxiosResponse, AxiosError } from 'axios';
import { STORE_UPDATE_ORDER, STORE_UPDATED_IMAGE, STORE_ORDERID, STORE_STATUS, STORE_USERCODE } from 'src/reducers/updateOrder';
import { STORE_SUCCESS_FILES_UPLOADED, STORE_SUCCESS_UPDATE, STORE_SUCCESS_UPDATE_MSG, STORE_ERROR_UPDATE, STORE_ERROR_UPDATE_MSG } from 'src/reducers/errors';

import { UpdateOrderStruct } from 'src/reducers/updateOrder';

export const getUpdateOrderData = (orderId: string | null, usercode: string | null) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {

        dispatch({ type: STORE_ORDERID, orderId } as AnyAction);
        dispatch({ type: STORE_USERCODE, usercode } as AnyAction);

        const responseOrder = await axios.post(
            `${getState().environment.API_URL}getOrderDetails?orderId=${orderId}`
        );
        const updateOrderData: UpdateOrderStruct[] = await responseOrder.data;
        // const updateOrderData = [updateOrderdata.find((elem) => {
        //     return elem.orderno === orderno;
        // })];

        dispatch({ type: STORE_UPDATE_ORDER, updateOrderData } as AnyAction);

        const status = updateOrderData[0]!!.status;
        dispatch({ type: STORE_STATUS, status } as AnyAction);

        return true;
    };
};
// export const onSaveuserCode = (usercode: string | null) => {
//     return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
//         dispatch({ type: STORE_USERCODE, usercode } as AnyAction);
//         console.error('user', getState().updateOrder.usercode);
//     };
// };
// export const onSaveOrderno = (orderno: string | null) => {
//     return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
//         dispatch({ type: STORE_ORDERNO, orderno } as AnyAction);
//         console.error('orderno', getState().updateOrder.orderno);
//     };
// };

export const onDropNewSmile = (newFile: Array<File>) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        const filterfile: Array<File> = [...getState().order.imagesWide].concat(newFile);

        const seen = new Set();
        const updatedImage = filterfile.filter(el => {
            const duplicate = seen.has(el.name);
            seen.add(el.name);
            return !duplicate;
        });

        if (updatedImage.length > 0) {
            const successFileUploadOpen: boolean = true;
            dispatch({ type: STORE_SUCCESS_FILES_UPLOADED, successFileUploadOpen } as AnyAction);

            dispatch({ type: STORE_UPDATED_IMAGE, updatedImage } as AnyAction);
            newFile.length = 0;
        }

    };
};
export const resetFinalfiles = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {

    if (getState().updateOrder.updatedImage > []) {
        const updatedImage: Array<File> = [];
        dispatch({ type: STORE_UPDATED_IMAGE, updatedImage } as AnyAction);
    }

};

export const onSaveStatusData = (event: React.ChangeEvent<{ value: number }>) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        const status = event.target.value;
        dispatch({ type: STORE_STATUS, status } as AnyAction);
    };
};

export const onDownloadUpdate = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {

    const dataImage = getState().updateOrder.updateOrderData[0];
    await FileSaver.saveAs(dataImage.normalsmilefile, `Normal_Smile_${dataImage.orderno}_${(new Date()).getTime()}.jpg`);
    await FileSaver.saveAs(dataImage.widesmailfile, `Wide_Smile_${dataImage.orderno}_${(new Date()).getTime()}.jpg`);

};

export const ondownloadUpdatestatus = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {

    // const staTus = getState().updateOrder.status;
    const dataImage = getState().updateOrder.updateOrderData[0];
    // const blob = new Blob([event.currentTarget.value], { type: 'image/jpeg' });
    // const file = new File([event.currentTarget.value], 'Smile.jpg', { type: 'image/jpeg' });
    // FileSaver.saveAs(file);
    FileSaver.saveAs(dataImage.normalsmilefile, `Normal_Smile_${dataImage.orderno}_${(new Date()).getTime()}.jpg`);

    await axios({
        method: 'post',
        url: `${getState().environment.API_URL}updateSmileOrderData`,
        data: {
            dsorderid: getState().updateOrder.updateOrderData[0].ds_ordersid,
            orderno: getState().updateOrder.updateOrderData[0].orderno,
            status: 1,
            usercode: getState().updateOrder.usercode
        }
    }).then(function (response: AxiosResponse) {
        const updatedImage: Array<File> = [];
        dispatch({ type: STORE_UPDATED_IMAGE, updatedImage } as AnyAction);
        const status = 1;
        dispatch({ type: STORE_STATUS, status } as AnyAction);
        const successUpdateOpen: boolean = true;
        dispatch({ type: STORE_SUCCESS_UPDATE, successUpdateOpen } as AnyAction);
        const successUpdatemsg: string = `Order number : ${getState().updateOrder.updateOrderData[0].orderno} has been accepted`;
        dispatch({ type: STORE_SUCCESS_UPDATE_MSG, successUpdatemsg } as AnyAction);
    }).catch(function (error: AxiosError) {
        console.error('err res', error);
        const errorUpdateOpen: boolean = true;
        dispatch({ type: STORE_ERROR_UPDATE, errorUpdateOpen } as AnyAction);
        const errorUpdatemsg: string = `Problem updating an order : ${getState().updateOrder.updateOrderData[0].orderno}`;
        dispatch({ type: STORE_ERROR_UPDATE_MSG, errorUpdatemsg } as AnyAction);
    });
    FileSaver.saveAs(dataImage.widesmailfile, `Wide_Smile_${dataImage.orderno}_${(new Date()).getTime()}.jpg`);
    return true;
};

export const UpdateNewFile = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {

    const finalSmile = getState().updateOrder.updatedImage[0];

    const formDataS = new FormData();
    formDataS.append('finalsmile', finalSmile);

    await axios({
        method: 'post',
        url: `${getState().environment.API_URL}finalSmileUpload`,
        data: formDataS,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/multipart/form-data',
            usercode: getState().updateOrder.usercode,
            servicetype: '1',
            dsorderid: getState().updateOrder.updateOrderData[0].ds_ordersid,
            design: '',
            msg: '',
            material: '',
            tat: '',

        },
    }).then(function (response: AxiosResponse) {
        // console.error('res', formDataS, response);
        const updatedImage: Array<File> = [];
        dispatch({ type: STORE_UPDATED_IMAGE, updatedImage } as AnyAction);
        const status = 2;
        dispatch({ type: STORE_STATUS, status } as AnyAction);
        const successUpdateOpen: boolean = true;
        dispatch({ type: STORE_SUCCESS_UPDATE, successUpdateOpen } as AnyAction);
        const successUpdatemsg: string = `Order number : ${getState().updateOrder.updateOrderData[0].orderno} has been updated successfully`;
        dispatch({ type: STORE_SUCCESS_UPDATE_MSG, successUpdatemsg } as AnyAction);
    }).catch(function (error: AxiosError) {
        console.error('err res', error, formDataS);
        const errorUpdateOpen: boolean = true;
        dispatch({ type: STORE_ERROR_UPDATE, errorUpdateOpen } as AnyAction);
        const errorUpdatemsg: string = `Problem with updating an order : ${getState().updateOrder.updateOrderData[0].orderno}`;
        dispatch({ type: STORE_ERROR_UPDATE_MSG, errorUpdatemsg } as AnyAction);
    });
    return true;
};
