import * as React from 'react';

export interface ErrorProps {
    onSubmit: () => void;
    errorTitle: string;
    errorMessage: string;
    errorBar: boolean;
    successMessage: string;
    successBar: boolean;
}

class Error extends React.Component<ErrorProps> {
    render() {
        return (
            <div>Error</div>
        );
    }
    componentWillMount() {
        // console.log("Test");
    }
}

export default Error;
