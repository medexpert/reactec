import { ReducersMapObject } from 'redux';
import { routerReducer } from 'react-router-redux';
import environment, { EnvironmentState } from './environment/Environment';
import errorState, { ErrorState } from './reducers/errors';
import order, { OrderState } from './reducers/order';
import updateOrder, { UpdateOrderState } from './reducers/updateOrder';
import viewOrder, { ViewOrderState } from './reducers/viewOrder';

export const reducers: ReducersMapObject = {
    environment,
    errorState,
    order,
    updateOrder,
    viewOrder,
    router: routerReducer,
};

export interface AppState {
    environment: EnvironmentState;
    errorState: ErrorState;
    order: OrderState;
    updateOrder: UpdateOrderState;
    viewOrder: ViewOrderState;
}

export const emptyAppState: () => AppState = () => ({
    environment: {},
    errorState: {},
    order: {
        cameraDisplayOption: 'none',
        photoDisplayOption: 'flex',
        smileDisplayOptions: 'flex',
        advancedDisplayOption: 'none',

        usercode: '',

        imagesNormal: [],
        imagesWide: [],
        filezip: [],
        imagedatawide: [],
        imagedatanormal: [],

        firstName: '',
        lastName: '',
        age: '',
        gender: ''
    },
    updateOrder: {
        updateOrderData: [{
            rowno: '',
            orderno: '',
            orderdate: '',
            filename: '',
            filesize: '',
            material: '',
            servicename: '',
            designtype: '',
            tat: '',
            msg: '',
            nounit: '',
            status: 0,
            normal_smile_url: '',
            wide_smile_url: '',
            final_smile_url: '',
            ds_ordersid: 0,
            qc_rating: '',
            qc_remarks: '',
            normalsmilefile: '',
            widesmailfile: '',
            finalsmilefile: '',
            p_name: '',
            p_age: '',
            p_gender: ''
        }],
        updatedImage: [],
        orderId: '',
        status: 0,
        usercode: ''
    },
    viewOrder: {
        viewOrderData: [{
            rowno: '',
            orderno: '',
            orderdate: '',
            filename: '',
            filesize: '',
            material: '',
            servicename: '',
            designtype: '',
            tat: '',
            msg: '',
            nounit: '',
            status: 0,
            normal_smile_url: '',
            wide_smile_url: '',
            final_smile_url: '',
            ds_ordersid: 0,
            normalsmilefile: '',
            widesmailfile: '',
            finalsmilefile: '',
            p_name: '',
            p_age: '',
            p_gender: ''
        }],
        orderId: '',
        usercode: ''
    }
});
