import * as React from 'react';
import { ReactNode } from 'react';
import { Route } from 'react-router-dom';
import { Redirect, Switch } from 'react-router';
import SmileAdvancedContainer from '../smileAdvanced/SmileAdvancedContainer';
import UpdateOrderContainer from '../UpdateOrder/UpdateOrderContainer';
import ViewOrderPageContainer from '../viewOrder/ViewOrderPageContainer';
import PreviewOrderPageContainer from '../previewOrder/PreviewOrderPageContainer';

class AppViewInitializer extends React.Component {
  render(): ReactNode {
    return (
      <Switch>
        <Route exact path="/smile" component={SmileAdvancedContainer} />
        <Route exact path="/updateorder" component={UpdateOrderContainer} />
        <Route exact path="/vieworder" component={ViewOrderPageContainer} />
        <Route exact path="/previewOrder" component={PreviewOrderPageContainer} />
        <Redirect to="/error" />
      </Switch>
    );
  }
}

export default AppViewInitializer;
