import * as React from 'react';
import { Theme, createStyles, makeStyles, withStyles } from '@material-ui/core/styles';
import { Radio, RadioProps, Typography, Grid, Button, List, ListItem, FormControl, InputLabel, Select, MenuItem, TextField, GridList, GridListTile, GridListTileBar } from '@material-ui/core';
import { blue } from '@material-ui/core/colors';
import WebcamScreenCapture from 'src/components/layouts/WebcamScreenCapture';

import Files from 'react-files';

export interface SmileAdvancedProps {
    AdvancedFileType: (selectedValue: string) => void;
    cameraDisplayOptions: string;
    photoDisplayOption: string;
    smileDisplayOptions: string;
    advancedDisplayOption: string;
    handleWideTakePhoto: (dataUri: string) => void;
    handleSmileTakePhoto: (dataUri: string) => void;
    onFilesChangeNormalSmile: (images: Array<File>) => void;
    onFilesChangeWideSmile: (images: Array<File>) => void;
    onFilesChangeStlzip: (fileszip: Array<File>) => void;
    onFilesErrorSmile: (error: Error, images: Array<File>) => void;
    onFilesErrorStlzip: (error: Error, fileszip: Array<File>) => void;
    imagesNormal: Array<File>;
    imagesWide: Array<File>;
    filezip: Array<File>;
    imagedatanormal: Array<File>;
    imagedatawide: Array<File>;

    onUploadImages: () => void;
    onUploadzip: () => void;
    resetFileValidateb2c: () => void;
    resetFileSmileb2c: () => void;
    createOrder: (selectedValuetype: string) => void;

    onSavePFirstName: (pfirstname: string) => void;
    onSavePLastName: (plastname: string) => void;
    onSavePAge: (page: string) => void;
    onSavePGender: (pgender: string) => void;

    pfirstname: string;
    plastname: string;
    page: string;
    pgender: string;
}

const BlueRadio = withStyles({
    root: {
        color: blue[400],
        '&$checked': {
            color: blue[600],
        },
    },
    checked: {},
})((props: RadioProps) => <Radio color="default" {...props} />);

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            textAlign: 'center',
            height: 'calc(100vh - 13px)',
            overflow: 'auto'
        },
        card: {
            display: 'flex',
        },
        details: {
            textAlign: 'center',
        },
        fontBold: {
            margin: theme.spacing(1),
            fontWeight: 400,
            borderBottom: '1px solid #3f51b5'
        },
        camera: {
            display: (props: SmileAdvancedProps) => props.cameraDisplayOptions,
            flexDirection: 'column',
        },
        file: {
            display: (props: SmileAdvancedProps) => props.photoDisplayOption,
            flexDirection: 'column',
        },
        fileUpload: {
            margin: theme.spacing(1),
            textAlign: 'center',
            fontSize: '30px',
            color: '#808080',
            height: '23vh',
            border: '2px solid #808080',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            borderStyle: 'dotted',
            transition: 'all 1s',
            '&:hover': {
                color: '#3f51b5',
                border: '2px solid #3f51b5'
            },
        },
        fileUploadzip: {
            margin: theme.spacing(1),
            textAlign: 'center',
            fontSize: '15px',
            color: '#808080',
            height: '20vh',
            border: '2px solid #808080',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            borderStyle: 'dotted',
            transition: 'all 1s',
            '&:hover': {
                color: '#3f51b5',
                border: '2px solid #3f51b5'
            },
        },
        imagetag: {
            top: '50%',
            left: '0',
            width: '100%',
            height: '100%',
            position: 'relative',
            transform: 'translateY(-50%)'
        },
        name: {
            textAlign: 'center',
            fontSize: '14px',
            color: '#3f51b5'
        },
        size: {
            textAlign: 'center',
            fontSize: '14px',
            color: '#3f51b5'
        },
        formControl: {
            margin: theme.spacing(1),
        },
        simple: {
            display: (props: SmileAdvancedProps) => props.smileDisplayOptions,
            flexDirection: 'column',
        },
        smilepro: {
            display: (props: SmileAdvancedProps) => props.advancedDisplayOption,
            flexDirection: 'column',
        },
        filesDropzoneActive: {
            border: '3px solid #3f51b5',
            color: '#3f51b5',
        },
        form: {
            maxWidth: '100%', // Fix IE 11 issue.
            margin: theme.spacing(1),
        },
        gridList: {
            width: 320,
            height: 180,
        },
        tilebar: {
            justifyContent: 'center',
            alignItems: 'center',
            margin: theme.spacing(1)
        },
        titleBar: {
            background:
                'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
                'rgba(0,0,0,0.1) 70%, rgba(0,0,0,0) 90%)',
        },

    }),
);

export const SmileAdvancedView: React.StatelessComponent<SmileAdvancedProps> = props => {
    const classes = useStyles(props);
    const [selectedValue, setSelectedValue] = React.useState('file');
    const [selectedValuetype, setSelectedValuetype] = React.useState('smile');
    const inputLabel = React.useRef<HTMLLabelElement>(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current!.offsetWidth);
    });

    return (
        <div className={classes.root}>
            <Grid container justify="center" alignItems="center" >
                <Grid item xs={12} sm={6} md={3}>
                    <Typography variant="h5" style={{ 'fontWeight': 'bold', 'color': 'black' }} className={classes.fontBold}>
                        Smile Order
                    </Typography>
                </Grid>
            </Grid>
            <div className={classes.form}>
                <Grid container justify="center" alignItems="center" spacing={1}>
                    <Grid item xs={12} sm={6} md={3}>
                        <TextField
                            size="small"
                            variant="outlined"
                            fullWidth
                            label="First Name"
                            autoFocus
                            onChange={event => props.onSavePFirstName(event.target.value)}
                            className={classes.formControl}
                            value={props.pfirstname}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={2}>
                        <TextField
                            size="small"
                            variant="outlined"
                            fullWidth
                            label="Last Name"
                            onChange={event => props.onSavePLastName(event.target.value)}
                            className={classes.formControl}
                            value={props.plastname}
                        />
                    </Grid>
                </Grid>
                <Grid container justify="center" alignItems="center" spacing={1}>
                    <Grid item xs={12} sm={4} md={1}>
                        <TextField
                            size="small"
                            variant="outlined"
                            fullWidth
                            label="Age"
                            type="number"
                            onChange={event => props.onSavePAge(event.target.value)}
                            className={classes.formControl}
                            value={props.page}
                        />
                    </Grid>
                    <Grid item xs={12} sm={4} md={2}>
                        <FormControl fullWidth size="small" variant="outlined" className={classes.formControl}>
                            <InputLabel ref={inputLabel} id="demo-simple-select-outlined">Gender</InputLabel>
                            <Select
                                fullWidth
                                labelWidth={labelWidth}
                                labelId="demo-simple-select-outlined"
                                id="demo-simple-select-outlined"
                                defaultValue=""
                                onChange={event => props.onSavePGender(event.target.value as string)}
                                value={props.pgender}
                            >
                                {/* <MenuItem value="" disabled>
                                    <em>None</em>
                                </MenuItem> */}
                                <MenuItem value="Male">Male</MenuItem>
                                <MenuItem value="Female">Female</MenuItem>
                                <MenuItem value="Others">Others</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={4} md={2}>
                        {/* <Typography variant="body1" color="primary" className={classes.fontBold}>
                        Type of Service
                    </Typography> */}
                        <FormControl fullWidth variant="outlined" size="small" className={classes.formControl}>
                            <InputLabel ref={inputLabel} id="demo-simple-select-outlined-label">Type of Service</InputLabel>
                            <Select
                                fullWidth
                                labelId="demo-simple-select-outlined-label"
                                id="demo-simple-select-outlined"
                                onChange={event => {
                                    setSelectedValuetype(event.target.value as string);
                                    props.createOrder(event.target.value as string);
                                }}
                                labelWidth={labelWidth}
                                defaultValue={selectedValuetype}
                            >
                                <MenuItem value="smile">Easy Smile</MenuItem>
                                <MenuItem value="smilepro">Easy Smile Pro</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
            </div>
            <div className={classes.simple}>
                <Grid container justify="center" alignContent="center" >

                    <Grid item xs={12} sm={8} md={4}>
                        <Typography variant="body1" color="primary" className={classes.fontBold}>
                            Upload Photos
                        </Typography>
                        <Typography variant="button" display="block" gutterBottom>
                            Choose Options:
                            <BlueRadio
                                checked={selectedValue === 'file'}
                                onChange={event => {
                                    setSelectedValue(event.target.value);
                                    props.AdvancedFileType(event.target.value);
                                }
                                }
                                defaultChecked
                                value="file"
                                name="radio-button-demo"
                            />
                            File &nbsp; |
                            <BlueRadio
                                checked={selectedValue === 'cam'}
                                onChange={event => {
                                    setSelectedValue(event.target.value);
                                    props.AdvancedFileType(event.target.value);
                                }
                                }
                                value="cam"
                                name="radio-button-demo"
                            /> Camera
                        </Typography>
                    </Grid>

                </Grid>
                <div className={classes.file}>
                    <Grid container justify="center" alignContent="center" >

                        <Grid item xs={12} sm={6} md={3} spacing={1}>
                            {props.imagesNormal.length === 1 ? null
                                : <Files
                                    className={classes.fileUpload}
                                    onChange={props.onFilesChangeNormalSmile}
                                    onError={props.onFilesErrorSmile}
                                    accepts={['image/*']}
                                    maxFiles={1}
                                    maxFileSize={10000000}
                                    minFileSize={0}
                                    dropActiveClassName={classes.filesDropzoneActive}
                                    clickable
                                >
                                    Normal Smile
                                </Files>
                            }
                            {
                                props.imagesNormal.length > 0
                                    ?
                                    <GridList spacing={1} className={classes.tilebar}>
                                        <GridListTile style={{ margin: '10px 0px', width: '90%' }}>
                                            {JSON.parse(JSON.stringify(props.imagesNormal[0])).preview.type === 'image'
                                                ? <img src={JSON.parse(JSON.stringify(props.imagesNormal[0])).preview.url} className={classes.imagetag} />
                                                : <div>{JSON.parse(JSON.stringify(props.imagesNormal[0])).extension}</div>}
                                            <GridListTileBar
                                                title="Normal Smile"
                                                titlePosition="top"
                                                actionPosition="left"
                                                className={classes.titleBar}
                                            />
                                        </GridListTile>
                                    </GridList>
                                    : null
                            }
                        </Grid>
                        <Grid item xs={12} sm={6} md={3} spacing={1}>
                            {props.imagesWide.length === 1 ? null
                                : <Files
                                    className={classes.fileUpload}
                                    onChange={props.onFilesChangeWideSmile}
                                    onError={props.onFilesErrorSmile}
                                    accepts={['image/*']}
                                    maxFiles={1}
                                    maxFileSize={10000000}
                                    minFileSize={0}
                                    dropActiveClassName={classes.filesDropzoneActive}
                                    clickable
                                >
                                    Wide Smile
                                </Files>
                            }
                            {
                                props.imagesWide.length > 0
                                    ?
                                    <GridList spacing={1}>
                                        <GridListTile style={{ margin: '10px 0px', width: '90%' }}>
                                            {JSON.parse(JSON.stringify(props.imagesWide[0])).preview.type === 'image'
                                                ? <img src={JSON.parse(JSON.stringify(props.imagesWide[0])).preview.url} className={classes.imagetag} />
                                                : <div>{JSON.parse(JSON.stringify(props.imagesWide[0])).extension}</div>}
                                            <GridListTileBar
                                                title="Wide Smile"
                                                titlePosition="top"
                                                actionPosition="right"
                                                className={classes.titleBar}
                                            />
                                        </GridListTile>
                                    </GridList>
                                    : null
                            }
                        </Grid>

                    </Grid>
                </div>
                <div className={classes.camera}>
                    <Grid container justify="center" alignContent="center" alignItems="center">

                        <Grid item xs={10} sm={5} md={3} spacing={1}>
                            <div className={classes.details}>
                                <Typography variant="button" display="block" gutterBottom>
                                    Normal Smile:
                                </Typography>
                                <WebcamScreenCapture
                                    onCameraCapture={props.handleSmileTakePhoto}
                                    videoId="smilephoto"
                                />
                            </div>
                        </Grid>
                        <Grid item xs={10} sm={5} md={3} spacing={1}>
                            <div className={classes.details}>
                                <Typography variant="button" display="block" gutterBottom>
                                    Wide Smile:
                                </Typography>
                                <WebcamScreenCapture
                                    onCameraCapture={props.handleWideTakePhoto}
                                    videoId="widephoto"
                                />
                            </div>
                        </Grid>

                    </Grid>
                </div>
                {selectedValuetype === 'smile' ?
                    <div style={{ 'display': 'flex', 'justifyContent': 'center', 'alignItems': 'center', 'alignContent': 'center' }}>
                        {((props.imagesNormal.length === 1 && props.imagesWide.length === 1 && selectedValuetype === 'smile' && selectedValue === 'file') ||
                            (props.imagedatanormal.length > 0 && props.imagedatawide.length > 0 && selectedValuetype === 'smile' && selectedValue === 'cam')) &&
                            (props.pfirstname !== '' && props.plastname !== '') ?
                            <Button style={{ 'margin': '5px', 'marginTop': '10px' }} size="small" variant="outlined" color="primary" onClick={props.onUploadImages}>Submit</Button>
                            :
                            <Button variant="outlined" style={{ 'margin': '5px', 'marginTop': '10px' }} size="small" disabled color="primary">Submit</Button>
                        }
                        {selectedValuetype === 'smile' ?
                            <Button variant="outlined" style={{ 'margin': '5px', 'marginTop': '10px' }} size="small" color="secondary" onClick={props.resetFileSmileb2c} >Reset</Button>
                            :
                            <Button variant="outlined" style={{ 'margin': '5px', 'marginTop': '10px' }} size="small" disabled color="secondary">Reset</Button>
                        }
                    </div>
                    : null}
            </div>
            <div className={classes.smilepro}>
                <Grid container justify="center" alignContent="center" >

                    <Grid item xs={12} sm={8} md={4}>
                        <Typography variant="body1" color="primary" className={classes.fontBold}>
                            Scan file upload: (.zip)
                        </Typography>
                    </Grid>

                </Grid>
                <Grid container justify="center" alignContent="center" >

                    <Grid item xs={12} sm={12} md={6}>
                        <div className="files">
                            {props.filezip.length > 0 ? null
                                : <Files
                                    className={classes.fileUploadzip}
                                    onChange={props.onFilesChangeStlzip}
                                    onError={props.onFilesErrorStlzip}
                                    accepts={['.zip']}
                                    multiple={false}
                                    maxFiles={1}
                                    maxFileSize={1000000000}
                                    minFileSize={0}
                                    clickable
                                    dropActiveClassName={classes.filesDropzoneActive}
                                >
                                    Scan file
                                </Files>
                            }
                            {
                                props.filezip.length > 0 ?
                                    <div>
                                        <List>{props.filezip.map((file: File) =>
                                            <ListItem key={JSON.parse(JSON.stringify(file)).id}>
                                                <div style={{ 'justifyContent': 'center' }} >
                                                    <div className={classes.name}>{file.name}</div>
                                                    <div className={classes.size}>{JSON.parse(JSON.stringify(file)).sizeReadable}</div>
                                                </div>
                                                {/* <div
                                                    id={JSON.parse(JSON.stringify(file)).id}
                                                // onClick={filesRemoveOne(file)}
                                                /> */}
                                            </ListItem>
                                        )}</List>
                                    </div>
                                    : null
                            }
                        </div>
                    </Grid>

                </Grid>
                <Grid container justify="center" alignContent="center" >

                    <Grid item xs={12} sm={6} md={3}>
                        {(((props.imagesNormal.length === 1 && props.imagesWide.length === 1 && selectedValuetype === 'smilepro' && selectedValue === 'file') ||
                            (props.imagedatanormal.length > 0 && props.imagedatawide.length > 0 && selectedValuetype === 'smilepro' && selectedValue === 'cam')) ||
                            (props.filezip.length > 0 && selectedValuetype === 'smilepro')) &&
                            (props.pfirstname !== '' && props.plastname !== '') ?
                            <Button style={{ 'margin': '5px', 'marginTop': '10px' }} variant="outlined" size="small" color="primary" onClick={props.onUploadzip}>Submit</Button>
                            :
                            <Button variant="outlined" style={{ 'margin': '5px', 'marginTop': '10px' }} disabled size="small" color="primary">Submit</Button>
                        }
                        {selectedValuetype === 'smilepro' ?
                            <Button variant="outlined" style={{ 'margin': '5px', 'marginTop': '10px' }} size="small" color="secondary" onClick={props.resetFileValidateb2c} >Reset</Button>
                            :
                            <Button variant="outlined" style={{ 'margin': '5px', 'marginTop': '10px' }} disabled size="small" color="secondary">Reset</Button>
                        }

                    </Grid>

                </Grid>
            </div>
        </div >
    );
};

export default SmileAdvancedView;
