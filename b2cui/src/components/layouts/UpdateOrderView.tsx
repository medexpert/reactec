import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { createStyles, makeStyles, Theme, withStyles } from '@material-ui/core/styles';
import { Button, Container, CssBaseline, Card, CardHeader, Avatar, CardContent, GridList, GridListTile, GridListTileBar } from '@material-ui/core';
import Files from 'react-files';
import ContactMailIcon from '@material-ui/icons/ContactMail';

import { UpdateOrderStruct } from 'src/reducers/updateOrder';

export interface UpdateOrderViewProps {
    updateOrderData: UpdateOrderStruct[];
    onDropNewSmile: (newFile: Array<File>) => void;
    updatedImage: Array<File>;
    status: number;

    onDownloadUpdatestatus: () => void;
    onDownloadUpdate: () => void;
    onDropNewSmileError: (error: Error, newFile: Array<File>) => void;
    onSaveStatus: (event: React.ChangeEvent<{ value: number }>) => void;
    onUpdateNewFile: () => void;

    resetFinalfilesmile: () => void;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexWrap: 'wrap',
            flexGrow: 1,
            justifyContent: 'space-around',
            // backgroundColor: theme.palette.background.paper,
            height: 'calc(100vh - 13px)',
            overflow: 'auto'
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120
        },
        selectEmpty: {
            marginTop: theme.spacing(1)
        },
        card: {
            maxWidth: 3500,
            marginTop: theme.spacing(2)
        },
        cardHeader: {
            padding: '5px',
            alignItems: 'center'
        },
        paper: {
            marginTop: theme.spacing(1),
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            // padding: theme.spacing(2),
            // textAlign: 'center',
            // color: theme.palette.text.secondary,
        },
        avatar: {
            margin: theme.spacing(1),
            backgroundColor: theme.palette.secondary.main
        },
        form: {
            maxWidth: '100%', // Fix IE 11 issue.
            // margin: theme.spacing(1),
        },
        textarea: {
            height: 10
        },
        forms: {
            width: '100%', // Fix IE 11 issue.
        },
        submit: {
            margin: theme.spacing(3, 0, 2)
        },
        gridList: {
            width: 600,
            height: 200,
            // width: 600,
            // height: 200,
        },
        titleBar: {
            background:
                'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
                'rgba(0,0,0,0.1) 70%, rgba(0,0,0,0) 90%)',
        },
        titlebar: {
            justifyContent: 'center',
            alignItems: 'center',
            // margin: theme.spacing(1),
        },
        icon: {
            color: 'white',
        },
        fileUpload: {
            textAlign: 'center',
            fontSize: '20px',
            color: '#808080',
            height: '25vh',
            // border: '2px solid #808080',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            borderStyle: 'dotted',
            transition: 'all .5s',
            // pointerEvents: (props: UpdateOrderViewProps) => {
            //     if (props.status === 2) {
            //         return 'auto';
            //     } else {
            //         return 'none';
            //     }
            // },
            '&:hover': {
                color: '#3f51b5',
                border: '2px solid #3f51b5'
            },
        },
        filesDropzoneActive: {
            border: '3px solid #3f51b5',
            color: '#3f51b5',
        },
        imagetag: {
            top: '50%',
            left: '0',
            width: '100%',
            height: '100%',
            position: 'relative',
            transform: 'translateY(-50%)'
        },
        name: {
            textAlign: 'center',
            fontSize: '14px',
            color: '#3f51b5'
        },
        size: {
            textAlign: 'center',
            fontSize: '14px',
            color: '#3f51b5'
        },
    }),
);
const CssTextField = withStyles({
    root: {
        '& .MuiInputBase-root.Mui-disabled': {
            color: '#000',
        },
    },
})(TextField);

const UpdateOrderView: React.StatelessComponent<UpdateOrderViewProps> = props => {

    const classes = useStyles(props);

    // const inputLabel = React.useRef<HTMLLabelElement>(null);
    // const [labelWidth, setLabelWidth] = React.useState(0);
    // React.useEffect(() => {
    //     setLabelWidth(inputLabel.current!.offsetWidth);
    // });
    // console.error('updateorder', props.updateOrderData);

    const typeService = ['None', 'Easy Smile', 'Easy Smile Pro'];
    const status = ['Order Created', 'Design in Progress', 'Design Completed'];

    return (
        <div className={classes.root}>
            <Container component="main" maxWidth="sm">
                <CssBaseline />
                <div className={classes.paper}>
                    <Grid container spacing={3}>
                        <Card className={classes.card}>
                            <CardHeader
                                avatar={
                                    <Avatar aria-label="recipe" className={classes.avatar}>
                                        <ContactMailIcon />
                                    </Avatar>}
                                title={<h2>Update Smile Order</h2>}
                            />
                            {
                                props.updateOrderData !== undefined ?

                                    <CardContent>
                                        <div className={classes.form}>
                                            <Grid container spacing={1}>
                                                <Grid item xs={12} sm={4}>
                                                    <CssTextField
                                                        variant="outlined"
                                                        disabled
                                                        fullWidth
                                                        label="Type of Service"
                                                        defaultValue=""
                                                        value={typeService[props.updateOrderData[0].servicename] || ''}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm={4}>
                                                    <CssTextField
                                                        variant="outlined"
                                                        disabled
                                                        fullWidth
                                                        label="Order No"
                                                        defaultValue=""
                                                        value={props.updateOrderData[0].orderno}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm={4}>
                                                    <CssTextField
                                                        variant="outlined"
                                                        disabled
                                                        fullWidth
                                                        label="Status"
                                                        defaultValue=""
                                                        value={status[props.status]}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm={6}>
                                                    <CssTextField
                                                        variant="outlined"
                                                        fullWidth
                                                        label="Patient Name"
                                                        disabled
                                                        value={props.updateOrderData[0].p_name}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm={3}>
                                                    <CssTextField
                                                        variant="outlined"
                                                        fullWidth
                                                        label="Age"
                                                        disabled
                                                        value={props.updateOrderData[0].p_age}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm={3}>
                                                    <CssTextField
                                                        variant="outlined"
                                                        fullWidth
                                                        label="Gender"
                                                        disabled
                                                        value={props.updateOrderData[0].p_gender}
                                                    />
                                                </Grid>
                                            </Grid>
                                        </div>
                                        <GridList spacing={2} style={{ justifyContent: 'center' }}>
                                            <GridListTile style={{ margin: '10px 0' }}>
                                                <a href={props.updateOrderData[0].normalsmilefile} target="_blank" download>
                                                    <img src={props.updateOrderData[0].normalsmilefile} alt="" className={classes.imagetag} />
                                                </a>
                                                <GridListTileBar
                                                    title="Normal Smile"
                                                    titlePosition="top"
                                                    actionPosition="left"
                                                    className={classes.titleBar}
                                                />
                                            </GridListTile>
                                            <GridListTile style={{ margin: '10px 0' }}>
                                                <a href={props.updateOrderData[0].widesmailfile} target="_blank" download>
                                                    <img src={props.updateOrderData[0].widesmailfile} alt="" className={classes.imagetag} />
                                                </a>
                                                <GridListTileBar
                                                    title="Wide Smile"
                                                    titlePosition="top"
                                                    actionPosition="left"
                                                    className={classes.titleBar}
                                                />
                                            </GridListTile>
                                        </GridList>

                                        <Grid container justify="space-around" className={classes.selectEmpty} spacing={2}>
                                            {status[props.status] === 'Order Created' ?
                                                <Button
                                                    style={{ 'transition': 'all .5s', 'margin': '5px' }}
                                                    size="small"
                                                    variant="outlined"
                                                    color="primary"
                                                    onClick={props.onDownloadUpdatestatus}
                                                >Accept Order
                                                </Button>
                                                :
                                                <Button
                                                    style={{ 'transition': 'all .5s', 'margin': '5px' }}
                                                    size="small"
                                                    variant="outlined"
                                                    color="primary"
                                                    onClick={props.onDownloadUpdate}
                                                >Download Images
                                                </Button>
                                            }
                                        </Grid>
                                        {
                                            status[props.updateOrderData[0].status] === 'Design in Progress' ?
                                                <GridList spacing={2} className={classes.titlebar}>
                                                    <GridListTile style={{ margin: '10px 0' }}>
                                                        {
                                                            props.updatedImage.length > 0
                                                                ?
                                                                <img className={classes.imagetag} src={JSON.parse(JSON.stringify(props.updatedImage[props.updatedImage.length - 1])).preview.url} />
                                                                : <Files
                                                                    className={classes.fileUpload}
                                                                    onChange={props.onDropNewSmile}
                                                                    onError={props.onDropNewSmileError}
                                                                    accepts={['image/*']}
                                                                    maxFiles={1}
                                                                    maxFileSize={10000000}
                                                                    minFileSize={0}
                                                                    dropActiveClassName={classes.filesDropzoneActive}
                                                                    clickable
                                                                >
                                                                    Designed Smile
                                                                </Files>
                                                        }
                                                        <GridListTileBar
                                                            title="Designed Smile"
                                                            titlePosition="top"
                                                            actionPosition="left"
                                                            className={classes.titleBar}
                                                        />
                                                    </GridListTile>
                                                </GridList>
                                                : null}
                                        <Grid container justify="space-around" className={classes.selectEmpty} spacing={2}>
                                            {props.updatedImage.length > 0 ?
                                                <Button style={{ 'margin': '5px' }} size="small" variant="outlined" color="primary" onClick={props.onUpdateNewFile}>Submit</Button>
                                                :
                                                null
                                            }
                                            {props.updatedImage.length > 0 ?
                                                <Button variant="outlined" style={{ 'margin': '5px' }} size="small" color="secondary" onClick={props.resetFinalfilesmile} >Reset</Button>
                                                :
                                                null
                                            }
                                        </Grid>
                                    </CardContent>
                                    : null}
                        </Card>
                    </Grid>
                </div>
            </Container>
        </div>
    );
};

export default UpdateOrderView;
