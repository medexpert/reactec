import * as React from 'react';
import { Button } from '@material-ui/core';

import './camera.css';

export interface WebcamCaptureProps {
    onCameraCapture: (dataUri: string) => void;
    videoId: string;
}

class WebcamScreenCapture extends React.Component<WebcamCaptureProps> {
    canvasId = `can_${this.props.videoId}`;
    imgId = `img_${this.props.videoId}`;
    videoId = this.props.videoId;
    render() {
        return (
            <div>
                <video style={{ 'margin': '5px', 'maxHeight': '135px', 'maxWidth': '240px' }} id={this.props.videoId} autoPlay />
                <img style={{ 'margin': '5px', 'maxHeight': '135px', 'maxWidth': '240px', 'display': 'none' }} id={this.imgId} src="" />
                <div style={{ 'display': 'flex', 'justifyContent': 'center'}}>
                    <Button
                        size="small"
                        style={{ 'margin': '5px', 'minWidth': '80px', 'maxWidth': '80px', 'maxHeight': '30px' }}
                        variant="contained"
                        color="secondary"
                        onClick={() => this.onScreenShot(this.props.videoId)}
                    >
                        Capture
                    </Button>
                    <Button
                        size="small"
                        style={{ 'margin': '5px', 'minWidth': '80px', 'maxWidth': '80px', 'maxHeight': '30px' }}
                        variant="contained"
                        color="secondary"
                        onClick={() => this.onClearScreen(this.props.videoId)}
                    >
                        Reset
                    </Button>

                </div>
            </div>
        );
    }

    hasGetUserMedia() {
        return !!(navigator.mediaDevices &&
            navigator.mediaDevices.getUserMedia);
    }

    onScreenShot(vid: string) {
        const videoId = `#${vid}`;
        const imageId = `#img_${vid}`;
        const canvas = document.createElement('canvas') as HTMLCanvasElement;
        const video = document.querySelector(videoId) as HTMLVideoElement;
        const img = document.querySelector(imageId) as HTMLImageElement;
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        const canvasContext = canvas.getContext('2d');
        canvasContext!!.drawImage(video, 0, 0);
        // Other browsers will fall back to image/png
        img.style.display = 'block';
        img.src = canvas.toDataURL('image/jpeg');
        video.style.display = 'none';
        const dataUri: string = canvas.toDataURL('image/jpeg');
        if (dataUri !== undefined) {
            this.props.onCameraCapture(dataUri);
        }

    }

    onClearScreen(vid: string) {
        const videoId = `#${vid}`;
        const imageId = `#img_${vid}`;

        const video = document.querySelector(videoId) as HTMLVideoElement;
        const img = document.querySelector(imageId) as HTMLImageElement;

        video.style.display = 'block';
        img.style.display = 'none';
        // window.location.reload(false);
        // const div = video.parentNode;
        // const test = document.createElement('video');
        // test.setAttribute('id', 'this.props.videoId');
        // test.setAttribute('autoplay', 'true');
        // document.appendChild(test);
    }

    componentDidMount() {
        if (this.hasGetUserMedia()) {
            const vgaConstraints = {
                // video: { width: { exact: 500 }, height: { exact: 375 } }
                video: { width: 240, height: 135 }
            };
            const video = document.querySelector(`#${this.props.videoId}`) as HTMLVideoElement;

            navigator.mediaDevices.getUserMedia(vgaConstraints).
                then((stream) => { video.srcObject = stream; });

        } else {
            console.error('Taking photo is not supported by your browser');
        }
    }
}

export default WebcamScreenCapture;
