import { connect } from 'react-redux';
import SmileAdvanced from './SmileAdvanced';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import {
    savePhotoOptions,
    saveCameraOptions,
    saveSimpleOptions,
    saveAdvancedOptions,
    onSaveuserCode,
    onFilesChangeNormalSmile,
    onFilesChangeWideSmile,
    onFilesChangeStlzip,
    onClearAllFilesSmileb2c,
    onClearAllFilesValidateb2c,
    onUploadImages,
    onUploadzip,
    onSaveCameraPicNormal,
    onSaveCameraPicWide,
    SavePFirstName,
    SavePLastName,
    SavePAge,
    SavePGender
} from 'src/actions/orderAction';
import { onFilesErrorSmile, onFilesErrorStlzip, closeError } from 'src/actions/errorAction';

export const mapStateToProps = (state: AppState) => {
    return {
        photoDisplayOption: state.order.photoDisplayOption,
        cameraDisplayOptions: state.order.cameraDisplayOption,
        smileDisplayOptions: state.order.smileDisplayOptions,
        advancedDisplayOption: state.order.advancedDisplayOption,
        imagesNormal: state.order.imagesNormal,
        imagesWide: state.order.imagesWide,
        filezip: state.order.filezip,
        imagedatanormal: state.order.imagedatanormal,
        imagedatawide: state.order.imagedatawide,

        successFileUploadOpen: state.errorState.successFileUploadOpen,
        successFileUploadTitle: state.errorState.successFileUploadTitle,
        successfilemsg: state.errorState.successfilemsg,

        errorfileopen: state.errorState.errorFileUploadedOpen,
        errorFileUploadTitle: state.errorState.errorFileUploadTitle,
        errorfilemsg: state.errorState.errorfilemsg,

        warningSubmitOpen: state.errorState.warningSubmitOpen,

        successSubmitOpen: state.errorState.successSubmitOpen,
        successSubmitmsg: state.errorState.successsubmitmsg,

        errorSubmitopen: state.errorState.errorSubmitOpen,
        errorSubmitmsg: state.errorState.errorsubmitmsg,

        pfirstname: state.order.firstName,
        plastname: state.order.lastName,
        page: state.order.age,
        pgender: state.order.gender,
    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        AdvancedFileType: (optionValue: string) => {
            if (optionValue === 'file') {
                dispatch(savePhotoOptions('flex'));
                dispatch(saveCameraOptions('none'));
            }
            if (optionValue === 'cam') {
                dispatch(savePhotoOptions('none'));
                dispatch(saveCameraOptions('flex'));
            }
        },
        createOrder: (optionValue: string) => {
            if (optionValue === 'smile') {
                dispatch(saveSimpleOptions('flex'));
                dispatch(saveAdvancedOptions('none'));
            }
            if (optionValue === 'smilepro') {
                dispatch(saveSimpleOptions('flex'));
                dispatch(saveAdvancedOptions('flex'));
            }

        },
        closeErrorfileUpload: () => {
            dispatch(closeError);
        },
        onComponentDidMount: (query: string | null) => {
            dispatch(saveSimpleOptions('flex'));
            dispatch(saveAdvancedOptions('none'));
            dispatch(savePhotoOptions('flex'));
            dispatch(saveCameraOptions('none'));
            dispatch(onSaveuserCode(query));
            dispatch(onClearAllFilesSmileb2c);
            dispatch(onClearAllFilesValidateb2c);
        },
        onUploadImages: () => {
            dispatch(onUploadImages);
        },
        onUploadzip: () => {
            dispatch(onUploadzip);
        },
        resetFileValidateb2c: () => {
            dispatch(onClearAllFilesValidateb2c);
        },
        resetFileSmileb2c: () => {
            dispatch(onClearAllFilesSmileb2c);
        },
        onFilesChangeNormalSmile: function (images: Array<File>) {
            dispatch(onFilesChangeNormalSmile(images));
        },
        onFilesChangeWideSmile: function (images: Array<File>) {
            dispatch(onFilesChangeWideSmile(images));
        },
        onFilesErrorSmile: function (error: Error, images: Array<File>) {
            dispatch(onFilesErrorSmile(error, images));
        },
        onFilesChangeStlzip: (filezip: Array<File>) => {
            dispatch(onFilesChangeStlzip(filezip));
        },
        onFilesErrorStlzip: (error: Error, fileszip: Array<File>) => {
            dispatch(onFilesErrorStlzip(error, fileszip));
        },
        handleSmileTakePhoto: (dataUri: string) => {
            dispatch(onSaveCameraPicNormal(dataUri));
        },
        handleWideTakePhoto: (dataUri: string) => {
            dispatch(onSaveCameraPicWide(dataUri));
        },
        onSavePFirstName: (pfirstname: string) => {
            dispatch(SavePFirstName(pfirstname));
        },
        onSavePLastName: (plastname: string) => {
            dispatch(SavePLastName(plastname));
        },
        onSavePAge: (page: string) => {
            dispatch(SavePAge(page));
        },
        onSavePGender: (pgender: string) => {
            dispatch(SavePGender(pgender));
        },
    };
};

const SmileAdvancedContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(SmileAdvanced);

export default SmileAdvancedContainer;
