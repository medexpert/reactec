import * as React from 'react';
import PreviewOrderView from '../layouts/PreviewOrderView';
import { RowData } from 'src/reducers/viewOrder';
// import { RowData } from 'src/reducers/draftorderfile';
// import { UserDetail } from '../../reducers/user';

export interface PreviewOrderPageViewProps {
    onComponentDidMount: (orderId: string | null, usercode: string | null) => void;
    PreviewOrderData: RowData[];
    onback: () => void;
}

class PreviewOrderPage extends React.Component<PreviewOrderPageViewProps> {
    render() {
        return (
            <PreviewOrderView PreviewOrderData={this.props.PreviewOrderData} onback={this.props.onback} />
        );
    }
    componentDidMount() {
        // const url: string = window.location.href;
        const urlParams: URLSearchParams = new URLSearchParams(window.location.search);
        const orderId: string | null = urlParams.get('orderId');
        const usercode: string | null = urlParams.get('userCode');
        this.props.onComponentDidMount(orderId, usercode);
        // this.props.onComponentDidMount();
    }
}

export default PreviewOrderPage;
