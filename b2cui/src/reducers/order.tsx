import { AnyAction, Reducer } from 'redux';

export const STORE_CAMERA_OPTIONS = 'easysmile/file-type-camera';
export const STORE_PHOTOFILE_OPTIONS = 'easysmile/file-type-upload';
export const STORE_SERVICE_TYPE_SIMPLE = 'easysmile/service-type-simple';
export const STORE_SERVICE_TYPE_ADVANCED = 'easysmile/sevice-type-advanced';
export const STORE_USERCODE = 'usercode/usercode-saved';

export const STORE_IMAGES_NORMAL = 'images/normal-smile';
export const STORE_IMAGES_WIDE = 'images/widw-smile';
export const STORE_IMAGE_DATA_NORMAL = 'imagedata/imagedata-normal';
export const STORE_IMAGE_DATA_WIDE = 'imagedata/imagedata-wide';
export const STORE_FILE_ZIP = 'stl-file/smile-zip';

export const STORE_PFIRSTNAME = 'patient/first-name';
export const STORE_PLASTNAME = 'patient/last-name';
export const STORE_PAGE = 'patient/age';
export const STORE_PGENDER = 'patient/gender';

export interface OrderState {
    cameraDisplayOption: string;
    photoDisplayOption: string;
    smileDisplayOptions: string;
    advancedDisplayOption: string;

    usercode: string;

    imagesNormal: Array<File>;
    imagesWide: Array<File>;
    filezip: Array<File>;
    imagedatanormal: Array<File>;
    imagedatawide: Array<File>;

    firstName: string;
    lastName: string;
    age: string;
    gender: string;
}

const initialState: OrderState = {
    cameraDisplayOption: 'none',
    photoDisplayOption: 'flex',
    smileDisplayOptions: 'flex',
    advancedDisplayOption: 'none',

    usercode: '',

    imagesNormal: [],
    imagesWide: [],
    filezip: [],
    imagedatawide: [],
    imagedatanormal: [],

    firstName: '',
    lastName: '',
    age: '',
    gender: '',
};

const reducer: Reducer<OrderState> = (state: OrderState = initialState, action: AnyAction): OrderState => {
    switch (action.type) {
        case STORE_CAMERA_OPTIONS:
            return {
                ...state,
                cameraDisplayOption: action.cameraDisplayOption,
            };
        case STORE_PHOTOFILE_OPTIONS:
            return {
                ...state,
                photoDisplayOption: action.photoDisplayOption,
            };
        case STORE_SERVICE_TYPE_SIMPLE:
            return {
                ...state,
                smileDisplayOptions: action.smileDisplayOptions,
            };
        case STORE_SERVICE_TYPE_ADVANCED:
            return {
                ...state,
                advancedDisplayOption: action.advancedDisplayOption,
            };
        case STORE_USERCODE:
            return {
                ...state,
                usercode: action.usercode
            };
        case STORE_IMAGES_NORMAL:
            return {
                ...state,
                imagesNormal: action.imagesNormal
            };
        case STORE_IMAGES_WIDE:
            return {
                ...state,
                imagesWide: action.imagesWide
            };
        case STORE_IMAGE_DATA_NORMAL:
            return {
                ...state,
                imagedatanormal: action.imagedatanormal
            };
        case STORE_IMAGE_DATA_WIDE:
            return {
                ...state,
                imagedatawide: action.imagedatawide
            };
        case STORE_FILE_ZIP:
            return {
                ...state,
                filezip: action.filezip
            };
        case STORE_PFIRSTNAME:
            return {
                ...state,
                firstName: action.firstName
            };
        case STORE_PLASTNAME:
            return {
                ...state,
                lastName: action.lastName
            };
        case STORE_PAGE:
            return {
                ...state,
                age: action.age
            };
        case STORE_PGENDER:
            return {
                ...state,
                gender: action.gender
            };
        default:
            return state;
    }
};

export default reducer;
