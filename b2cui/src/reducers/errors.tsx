import { AnyAction, Reducer } from 'redux';
export const STORE_ERROR_TITLE = 'error/errorTitle';
export const STORE_ERROR_MESSAGE = 'error/errorMessage';
export const STORE_ERROR_BAR = 'error/errorBar';
export const STORE_ERROR_DETAILS = 'error/errorDetails';
export const STORE_SUCCESS_TITLE = 'success/successTitle';
export const STORE_SUCCESS_MESSAGE = 'success/successMessage';
export const STORE_SUCCESS_BAR = 'success/successBar';
export const STORE_ERROR_FILES_UPLOADED = 'error/unsupportedformat';
export const STORE_SUCCESS_FILES_UPLOADED = 'success/addedSuccessfully';
export const STORE_WARN_FILES_UPLOADED = 'warning/dupicates-detected';
export const STORE_WARN_FILES_SUBMIT = 'warning/submit-condition';
export const STORE_SUCCESS_SUBMIT = 'success/submit-success';
export const STORE_SUCCESS_SUBMIT_MSG = 'success/submit-success-messege';
export const STORE_ERROR_SUBMIT = 'error/submit-error';
export const STORE_ERROR_SUBMIT_MSG = 'error/submit-error-messege';
export const STORE_SUCCESS_UPDATE = 'success/Update-success';
export const STORE_SUCCESS_UPDATE_MSG = 'success/Update-success-messege';
export const STORE_ERROR_UPDATE = 'error/Update-error';
export const STORE_ERROR_UPDATE_MSG = 'error/Update-error-messege';

export interface ErrorState {
    errorTitle?: String;
    errorMessage?: String;
    errorBar?: boolean;
    successTitle?: string;
    successMessage?: string;
    successBar?: boolean;
    
    errorFileUploadedOpen?: boolean;
    errorFileUploadTitle?: string;
    errorfilemsg?: string;
    successFileUploadOpen?: boolean;
    successFileUploadTitle?: string;
    successfilemsg?: string;
    warnFileUploadOpen?: boolean;
    warnFileUploadTitle?: string;
    warnfilemsg?: string;
    successSubmitOpen?: boolean;
    successsubmitmsg?: string;
    errorSubmitOpen?: boolean;
    errorsubmitmsg?: string;
    successUpdateOpen?: boolean;
    successUpdatemsg?: string;
    errorUpdateOpen?: boolean;
    errorUpdatemsg?: string;
    warningSubmitOpen?: boolean;

}

const initialState: ErrorState = {
    errorTitle: 'There\'s been a technical problem',
    errorMessage: 'Initialization failed',
    errorBar: false,
    successTitle: 'Success',
    successMessage: 'Initialization success',
    successBar: false,

    errorFileUploadedOpen: false,
    errorFileUploadTitle: 'Error',
    errorfilemsg: 'Problem with adding the file, Please try again',

    successFileUploadOpen: false,
    successFileUploadTitle: 'Success',
    successfilemsg: 'Files got added successfully',

    warnFileUploadOpen: false,
    warnfilemsg: 'Dupicates got removed',

    successSubmitOpen: false,
    successsubmitmsg: 'Your order has been placed successfully',

    errorSubmitOpen: false,
    errorsubmitmsg: 'Error with placing your order',

    successUpdateOpen: false,
    successUpdatemsg: 'Your order has been placed successfully',

    errorUpdateOpen: false,
    errorUpdatemsg: 'Error with placing your order',

    warningSubmitOpen: false,

};

const reducer: Reducer<ErrorState> =
    (state: ErrorState = initialState, action: AnyAction): ErrorState => {
        switch (action.type) {
            case STORE_ERROR_TITLE:
                return {
                    ...state,
                    errorTitle: action.errorTitle
                };
            case STORE_ERROR_MESSAGE:
                return {
                    ...state,
                    errorMessage: action.errorMessage
                };
            case STORE_ERROR_BAR:
                return {
                    ...state,
                    errorBar: action.errorBar
                };
            case STORE_SUCCESS_TITLE:
                return {
                    ...state,
                    successTitle: action.successTitle
                };
            case STORE_SUCCESS_MESSAGE:
                return {
                    ...state,
                    successMessage: action.successMessage
                };
            case STORE_SUCCESS_BAR:
                return {
                    ...state,
                    successBar: action.successBar
                };
            case STORE_ERROR_FILES_UPLOADED:
                return {
                    ...state,
                    errorFileUploadedOpen: action.errorFileUploadedOpen
                };
            case STORE_SUCCESS_FILES_UPLOADED:
                return {
                    ...state,
                    successFileUploadOpen: action.successFileUploadOpen
                };
            case STORE_WARN_FILES_UPLOADED:
                return {
                    ...state,
                    warnFileUploadOpen: action.warnFileUploadOpen
                };
            case STORE_WARN_FILES_SUBMIT:
                return {
                    ...state,
                    warningSubmitOpen: action.warningSubmitOpen
                };
            case STORE_SUCCESS_SUBMIT:
                return {
                    ...state,
                    successSubmitOpen: action.successSubmitOpen
                };
            case STORE_SUCCESS_SUBMIT_MSG:
                return {
                    ...state,
                    successsubmitmsg: action.successsubmitmsg
                };
            case STORE_ERROR_SUBMIT:
                return {
                    ...state,
                    errorSubmitOpen: action.errorSubmitOpen
                };
            case STORE_ERROR_SUBMIT_MSG:
                return {
                    ...state,
                    errorsubmitmsg: action.errorsubmitmsg
                };
            case STORE_SUCCESS_UPDATE:
                return {
                    ...state,
                    successUpdateOpen: action.successUpdateOpen
                };
            case STORE_SUCCESS_UPDATE_MSG:
                return {
                    ...state,
                    successUpdatemsg: action.successUpdatemsg
                };
            case STORE_ERROR_UPDATE:
                return {
                    ...state,
                    errorUpdateOpen: action.errorUpdateOpen
                };
            case STORE_ERROR_UPDATE_MSG:
                return {
                    ...state,
                    errorUpdatemsg: action.errorUpdatemsg
                };
            default:
                return state;
        }
    };
export default reducer;
