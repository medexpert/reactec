import { AnyAction, Reducer } from 'redux';
export const STORE_ERROR_TITLE = 'error/errorTitle';
export const STORE_ERROR_MESSAGE = 'error/errorMessage';
export const STORE_ERROR_STATE = 'error/errorState';
export const STORE_ERROR_DETAILS = 'error/errorDetails';

export interface ErrorDetails {
    errorDescription?: String;   
    error?: String;
    errorCause?: string;
}

export interface ErrorState {
    errorDetails?: ErrorDetails;
    errorTitle?: String;
    errorMessage?: String;   
    errorStates?: String;
}

const initialState: ErrorState = {
    errorDetails: {
        errorDescription: 'Something went wrong in your route URL.',
        error: 'ROUTE_ERROR'
    }, 
    errorTitle: 'There\'s been a technical problem',
    errorMessage: 'Initialization failed',
    errorStates: 'ROUTE_ERROR'
};

const reducer: Reducer<ErrorState> =
    (state: ErrorState = initialState, action: AnyAction): ErrorState => {
        switch (action.type) {
            case STORE_ERROR_DETAILS:
            return {
                ...state,
                errorDetails: action.errorDetails,
            };
            case STORE_ERROR_TITLE:
            return{
                ...state,
                errorTitle: action.errorTitle
            };
            case STORE_ERROR_MESSAGE:
            return{
                ...state,
                errorMessage: action.errorMessage
            };
            case STORE_ERROR_STATE:
            return{
                ...state,
                errorStates: action.errorStates
            };
            default:
                return state;
        }
    };
export default reducer;
