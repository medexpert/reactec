import * as React from 'react';

export interface LoginPageProps {
    onComponentDidMount: (url: string ) => void;
    onSubmit: () => void;
    btnElement: string;
    btnType: string;

}

class LoginPage extends React.Component<LoginPageProps> {
    render() {
        return (
            <div>
                <div className="row">&nbsp;</div>
                <div className="row">
                    <div className="col-12 col-md-8">
                        Login Page
                        <br/>
                            <button onClick={this.props.onSubmit}>Login Here</button>
                    </div>
                </div>
            </div>
        );
    }
    componentDidMount() {
        const url: string = window.location.href;
        this.props.onComponentDidMount(url);
    }
}

export default LoginPage;
