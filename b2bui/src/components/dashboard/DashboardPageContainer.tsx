import { connect } from 'react-redux';
import DashboardPage from './DashboardPage';
import { Dispatch } from 'redux';
import { AppState } from '../../redux';
import { push } from 'react-router-redux';

export const mapStateToProps = (state: AppState) => {
    return {
        btnElement: 'Sign-in to Troubleshoot',
        btnType: 'signin',
    };
};

export const mapDispatchToProps = (dispatch: Dispatch<AppState>) => {
    return {
        onSubmit: () => {
            // dispatch(fetchCurrentUser);
            dispatch(push('/app/addorder'));
        },
        onComponentDidMount:  (url: string) => {
            // dispatch(initiateLogin);
        }
    };
};

const DashboardPageContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(DashboardPage);

export default DashboardPageContainer;
