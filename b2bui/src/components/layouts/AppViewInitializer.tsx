import * as React from 'react';
import { ReactNode } from 'react';
import { Route } from 'react-router-dom';
import { Redirect, Switch } from 'react-router';
import LoginPageContainer from '../login/LoginContainer';
import LandingPageContainer from '../landingPage/LandingPageContainer';

class AppViewInitializer extends React.Component {
    render(): ReactNode {
        return (
            <Switch>
                <Route exact path="/login" component={LoginPageContainer}/>
                <Route path="/app" component={LandingPageContainer}/>
                <Redirect exact from="/" to="/login"/>
                <Redirect to="/error"/>
            </Switch>
        );
    }
}

export default AppViewInitializer;
