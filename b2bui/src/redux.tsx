import { ReducersMapObject } from 'redux';
import { routerReducer } from 'react-router-redux';
import user, { UserState } from './reducers/user';
import environment, { EnvironmentState } from './environment/Environment';
import errorState, { ErrorState } from './reducers/errors';
import  basic, { BasicState } from './reducers/basic';

export const reducers: ReducersMapObject = {
    user,
    environment,
    errorState,
    basic,
    router: routerReducer,
};

export interface AppState {
    user: UserState;
    environment: EnvironmentState;
    errorState: ErrorState;
    basic: BasicState;
}

export const emptyAppState: () => AppState = () => ({
    user: {},
    environment: {},
    errorState: {},
    basic: {},
});
