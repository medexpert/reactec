import { push } from 'react-router-redux';
import { AnyAction, Dispatch } from 'redux';
import { AppState } from '../redux';
import { 
    STORE_ERROR_MESSAGE, 
    STORE_ERROR_TITLE, 
    STORE_ERROR_STATE } from '../reducers/errors';    

export const loadErrorPage = 
    ( errorMessage: string, errorTitle: string, errorStates: String ) => { 
        return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {                
        dispatch({ type: STORE_ERROR_MESSAGE, errorMessage } as AnyAction); 
        dispatch({ type: STORE_ERROR_TITLE, errorTitle } as AnyAction); 
        dispatch({ type: STORE_ERROR_STATE, errorStates } as AnyAction); 
        dispatch(push('/error'));
        return true;
    };
};
