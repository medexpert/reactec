import {
    STORE_EID_NUMBER,
    STORE_ICCID_NUMBER,
    CIP_ACCESS_TOKEN,
    CIP_EXPIRES_IN,
    CIP_TOKEN_TYPE,
    STORE_ICCID_NUMBERS,
    IS_MULTIPLE_ICCID
} from '../reducers/basic';
import { push } from 'react-router-redux';
import { AnyAction, Dispatch } from 'redux';
import { AppState } from '../redux';
import { SUCCEED_FETCH_USER, UserDetail, ESimProfiles, SELECT_USER_PROFILE } from '../reducers/user';
import { STORE_ERROR_DETAILS, STORE_ERROR_TITLE } from '../reducers/errors';
import { initiateLogin } from './userAction';

export const saveEidNumber = (eidNumber: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: STORE_EID_NUMBER, eidNumber } as AnyAction);
        return true;
    };
};

export const updateIsMultipleIccids = (isMultipleIccids: boolean) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: IS_MULTIPLE_ICCID, isMultipleIccids } as AnyAction);
        return true;
    };
};

export const saveIccidNumbers = (iccidNumbers: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        const iccids: string[] = iccidNumbers.split(',');
        dispatch({ type: STORE_ICCID_NUMBERS, iccids } as AnyAction);
        return true;
    };
};

export const saveIccidNumber = (iccidNumber: string) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: STORE_ICCID_NUMBER, iccidNumber } as AnyAction);
        return true;
    };
};

export const saveSelectedProfile = (selectedEsimProfiles: ESimProfiles) => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: SELECT_USER_PROFILE, selectedEsimProfiles } as AnyAction);
        return true;
    };
};

export const notValidDevice = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    const errorTitle = 'Your device is not supported for the eSIM Support App.';
    const errorDetails = {
        errorDescription: 'Please try with an eSIM capable device.',
        error: 'ROUTE_ERROR'
    };
    dispatch({ type: STORE_ERROR_TITLE, errorTitle } as AnyAction);
    dispatch({ type: STORE_ERROR_DETAILS, errorDetails } as AnyAction);
    dispatch(push('/error'));
};

export const startFetchUserDetails = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    dispatch(push('/userInfo'));
};

export const doTroubleShoot = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    const response = await fetch(
        `${getState().environment.ENV_MODE}getUserDetails?` +
        `eid=${getState().basic.eidNumber}`,
        {
            mode: 'cors',
            method: 'GET',
            headers: {
                credentials: 'include',
                'accessToken': `${getState().basic.cipAccessToken}`,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE'
            }
        }
    );

    if (response.status === 500) {
        const errorDetails = await response.json();
        const errors = errorDetails.error;
        let errorTitle = 'There\'s been a technical problem';
        if (errors === 'EXCEEDED_REQUEST_ERROR') {
            errorTitle = 'Download limit exceeded!';
        }
        dispatch({ type: STORE_ERROR_TITLE, errorTitle } as AnyAction);
        dispatch({ type: STORE_ERROR_DETAILS, errorDetails } as AnyAction);
        // updateNewRelicStatusAttributes(errorTitle, errorDetails.errorCause, errors);
        if (errors === 'TDI_LINK_ERROR') {
            dispatch(push('/accountError'));
        } else {
            dispatch(push('/error'));
        }
    } else {
        const userDetails: UserDetail = await response.json();

        dispatch({ type: SUCCEED_FETCH_USER, userDetails } as AnyAction);
        const len = userDetails.esimProfiles!!.length;
        if (len > 1) {
            const getIccids = getState().basic.iccids || 'NA';
            const getIccid = getState().basic.iccidNumber || 'NA';
            for (let i = 0; i <= len - 1; i++) {
                let isSelected: boolean = false;
                const curEsimProfile: ESimProfiles = userDetails.esimProfiles!![i];
                const iccid = curEsimProfile.iccid;
                // console.error(`ProfileIccid: ${iccid} :::: ${getIccids} ==> ${getIccid} ==> ${getState().basic.isMultipleIccids}`);
                if (getState().basic.isMultipleIccids) {
                    // console.error(`${getIccids!!.indexOf(iccid)}`);
                    if (getIccids!!.indexOf(iccid) === -1) {
                        userDetails.esimProfiles!![i].isInstalled = true; // not in device
                        if (!isSelected) {
                            isSelected = true;
                            dispatch(saveSelectedProfile(userDetails.esimProfiles!![i]));
                        }
                    }
                } else {
                    // console.error(`${getIccid!!.indexOf(iccid)}`);
                    if (getIccid!!.indexOf(iccid) === -1) {
                        userDetails.esimProfiles!![i].isInstalled = true;
                        if (!isSelected) {
                            isSelected = true;
                            dispatch(saveSelectedProfile(userDetails.esimProfiles!![i]));
                        }
                    }
                }

            }
            // console.error(JSON.stringify(userDetails));
            dispatch({ type: SUCCEED_FETCH_USER, userDetails } as AnyAction);
            dispatch(push('/list'));
        } else {
            userDetails.esimServiceNumber = userDetails!!.esimProfiles!![0].esimServiceNumber || '';
            userDetails.iccid = userDetails.esimProfiles!![0].iccid;
            const selectedEsimProfiles: ESimProfiles = userDetails.esimProfiles!![0];
            dispatch({ type: SUCCEED_FETCH_USER, userDetails } as AnyAction);
            dispatch({ type: SELECT_USER_PROFILE, selectedEsimProfiles } as AnyAction);
            dispatch(push('/download'));
        }
    }
    return true;
};

export const fetchCurrentUserInfo = () => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch(push('/download'));
    };
};

export const fetchCurrentUserDetails =
    (cipAccessToken: string, cipTokenType: string, cipExpiresIn: string) => {
        return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
            dispatch({ type: CIP_ACCESS_TOKEN, cipAccessToken } as AnyAction);
            dispatch({ type: CIP_TOKEN_TYPE, cipTokenType } as AnyAction);
            dispatch({ type: CIP_EXPIRES_IN, cipExpiresIn } as AnyAction);
            dispatch(doTroubleShoot);
            return true;
        };
    };

export const downloadProfile = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    dispatch(push('/downloadInitiated'));
};

export const initiateDownload = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    const response = await fetch(
        `${getState().environment.ENV_MODE}initiateDownload?` +
        `eid=${getState().basic.eidNumber}` +
        `&iccid=${getState().user.selectedEsimProfiles!!.iccid}`,
        {
            mode: 'cors',
            method: 'GET',
            headers: {
                credentials: 'include',
                'accessToken': `${getState().basic.cipAccessToken}`,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE'
            }
        });

    if (response.status === 500) {
        const errorDetails = await response.json();
        var errorTitle = 'There\'s been a technical problem';
        const errors = errorDetails.error;
        if (errors === 'EXCEEDED_REQUEST_ERROR') {
            errorTitle = 'Download limit exceeded!';
        }
        dispatch({ type: STORE_ERROR_TITLE, errorTitle } as AnyAction);
        dispatch({ type: STORE_ERROR_DETAILS, errorDetails } as AnyAction);
        // updateNewRelicStatusAttributes(errorTitle, errorDetails.errorCause, errors);
        if (errors === 'TDI_LINK_ERROR') {
            dispatch(push('/accountError'));
        } else {
            dispatch(push('/error'));
        }
    } else {
        const acivationDetails = await response.json();
        if (getState().environment.ENV_MODE === 'prod') {
            window.location.replace(
                `${getState().environment.ENV_MODE}?` +
                `eid=${getState().basic.eidNumber}` +
                `&iccid=${acivationDetails.iccid}` +
                `&activationCode=${acivationDetails.activationCode}`);
        } else {
            window.location.replace(
                `${getState().environment.ENV_MODE}?` +
                `transactionId=${acivationDetails.transactionId}` +
                `&iccid=${acivationDetails.iccid}` +
                `&activationCode=${acivationDetails.activationCode}`);
        }

    }
};

export const retryFromException = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    const errorState = `${getState().errorState.errorDetails!!.error}`;
    // const imgElement = document.getElementById('logo') as HTMLElement;
    // imgElement.style.visibility = 'visible';
    if (errorState.trim().toLowerCase() === 'troubleshoot_error') {
        const retryUrl = `/callback#access_token=${getState().basic.cipAccessToken}` +
            `&token_type=${getState().basic.cipTokenType}` +
            `&expires_in=${getState().basic.cipExpiresIn}`;
        dispatch(push(retryUrl));
    } else {
        dispatch(downloadProfile);
    }
};

export const reLoginTdi = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    dispatch(initiateLogin);
};

export const doLogout = () => {
    return async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
        dispatch({ type: CIP_ACCESS_TOKEN, cipAccessToken: '' } as AnyAction);
        dispatch({ type: CIP_TOKEN_TYPE, cipTokenType: '' } as AnyAction);
        dispatch({ type: CIP_EXPIRES_IN, cipExpiresIn: '' } as AnyAction);
        return true;
    };
};
