import { Dispatch } from 'redux';
import { AppState } from '../redux';

export const initiateLogin = async (dispatch: Dispatch<AppState>, getState: () => AppState) => {
    const url = document.location!!.href;
    let loadUrl = url;
    var cnt = 0;
    var ind = 0;
    while (cnt !== 3) {
        ind = loadUrl.indexOf('/', ind) + 1;
        cnt++;
    }
    loadUrl = loadUrl.substring(0, ind);

    const callbackUrl = loadUrl;
    let responseType: string | undefined = getState().environment.ENV_MODE;
    responseType = responseType!!.split('-').join(' ');
    const loginUrl = `${getState().environment.ENV_MODE}` +
                    `?client_id=${getState().environment.ENV_MODE}` +
                    `&redirect_uri=${callbackUrl}callback` +
                    `&response_type=${responseType}` +
                    `&scope=app.esim_support`;

    window.location.replace(loginUrl || '');
};
